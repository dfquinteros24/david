from keras import Sequential
from keras.layers import CuDNNLSTM
from keras.layers import Dense, TimeDistributed

# Importamos Embedding, GAP1D Layers
from keras.layers import Embedding, GlobalAveragePooling1D

from keras.layers import Dropout, GlobalAveragePooling2D
from tensorflow.python.keras import optimizers
from keras.applications import MobileNetV2
from keras import backend as K
import tensorflow as tf
from keras.models import Model


def create_model_pretrain(dim, n_sequence, n_channels, n_output):
    '''
    Paremeter:
        dim -- dimension of image, use (224,224) for MobileNetV2
        n_sequence -- number of sequence(timestep) for LSTM
        n_channels -- number of color channels, use 3 for RGB
        n_output -- number of output class

    '''

    """
    # Create a custom standardization function to strip HTML break tags '<br />'.
    def custom_standardization(input_data):
        lowercase = tf.strings.lower(input_data)
        stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
        return tf.strings.regex_replace(stripped_html,
                                        '[%s]' % re.escape(string.punctuation), '')

    # Vocabulary size and number of words in a sequence.
    vocab_size = 10000
    sequence_length = 100

    # Use the text vectorization layer to normalize, split, and map strings to
    # integers. Note that the layer uses the custom standardization defined above.
    # Set maximum_sequence length as all samples are not of the same length.
    vectorize_layer = TextVectorization(
        standardize=custom_standardization,
        max_tokens=vocab_size,
        output_mode='int',
        output_sequence_length=sequence_length)

    # Make a text-only dataset (no labels) and call adapt to build the vocabulary.
    text_ds = train.train().map(lambda x, y: x)
    vectorize_layer.adapt(text_ds)
    """

    model = Sequential([


      #vectorize_layer,
      #Embedding(4, 4, name="embedding")
      #GlobalAveragePooling1D(),
      #Dense(8, activation='relu'),
      #Dense(1)

    ])

    model.add(
        TimeDistributed(
            MobileNetV2(weights='imagenet',include_top=False),
            input_shape=(n_sequence, *dim, n_channels)
        )
    )
    model.add(
        TimeDistributed(
            GlobalAveragePooling2D()
        )
    )
    model.add(CuDNNLSTM(64, return_sequences=False))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(.5))
    model.add(Dense(24, activation='relu'))
    model.add(Dropout(.5))
    model.add(Dense(n_output, activation='softmax'))


    model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(from_logits=True), metrics=['sparse_categorical_accuracy','accuracy','mean_squared_error'])


    return model

